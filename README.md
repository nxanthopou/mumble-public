# Introduction  

Greetings,  
this is a repository hosting software and schematics of a cellular phone.  

# Modules  

## Hardware  
* Teensy 4.0  
* SIM800L  
* 1.5" RGB 128x128 OLED module  
* 3.7V, 1350mAh LiPo battery
* 4 buttons  
* 1 potentiometer  
* 1 8Ω speaker 
* 1 electret microphone 

The schematic is available in `schematics/dref/exports/`  

## Software  
* FreeRTOS https://github.com/discord-intech/FreeRTOS-Teensy4  
* SSD1351 https://github.com/kirberich/teensy_ssd1351  
* Bounce2 downloaded from Arduino IDE  
* SPI built-in  

# Important notes
* OLED library's benchmarks indicate ~13ms to update the screen with LowColor setting, ~24ms HighColor. It is set by `TOLED_MS` in `definitions.h`.  
* Teensy 4.0 has 3 GND pins. Teensy's creator, Paul Stoffregen states -noise wise- in PJRC's forums (https://forum.pjrc.com/threads/57882-Does-Teensy-4-0-have-an-analog-ground-pin):  

> They should all be pretty similar, since layer #2 inside the PCB is a ground plane and the chip has many GND pins.  
> But if you had to choose one, I'd suggest going with the GND on the right hand side (next to the Program pin).  


# Roadmap  
* Add optional end-to-end encryption between two devices. The signal captured from microphone will be encrypted, transmitted over SIM800, and decrypted before driving the speaker.     
* Voltage stabilizers - SIM800 draws up to 2 amperes of current. A voltage drop (~0.10-0.20 volts) across the circuit can be noticed when a call is made.  
* Add send-receive-retrieve-delete-show SMS functionalities 


# Developers  
* Nikitas Xanthopoulos https://bitbucket.org/nxanthopou
* Athanasios Kostis https://bitbucket.org/thanosCebolitas/

# License  
GNU GPL v3
