/*
    DIN/MOSI 11
    SCLK 13
    DC 15
    CS 10
    RESET 14
*/
void init_oled(void);
void draw_upper_screen(void);
void draw_idle_screen(void);
void draw_inc_call_screen(void);
void draw_call_in_prg_screen(void);
void draw_typing_number_screen(void);
void draw_animated_circles(void);
void draw_jamming_icon(void);

void draw_unlock_screen() {
  display.fillScreen(black);
  display.setFont(OLED_FONT_BIG_BOLD);
  display.setCursor(0, 45);
  display.setTextColor(white);
  display.print(F("PIN:"));

  display.setFont(OLED_FONT_BIG_BOLD);

  display.setCursor(0, 90);
  display.setTextColor(yellow);
  display.print((char*)sim_pin);

  display.fillRect(sim_pin_digit * FONT_BIG_PT, 95,  20, 3, red);

  display.fillRoundRect(95, 61,  28, 36, 2, red);
  display.fillRoundRect(98, 64,  22, 30, 2, grayBlue);
  display.setTextColor(red);
  display.setCursor(100, 90);
  display.print(pot_value);
  display.setFont(OLED_FONT);

  display.updateScreen();
}

/*
   Function: draw_upper_screen
   ----------------------------
     Draws the upper part of the screen,
      operator (string)
      signal quality (icon)
      battery capacity (icon)
      state of the phone (icon)
      jamming detection (icon)
     needs a display.update() to actually
     control the display.
*/
void draw_upper_screen() {
  Color signalCol = ssd1351::RGB(225, 125, 0);
  Color batteryCol = ssd1351::RGB(45, 125, 0);
  char net_op_chopped[9];
  int16_t mapped_signal, mapped_battery;

  display.fillScreen(black);
  display.setTextColor(yellow);

  // network operator
  display.setCursor(0, 15);
  strncpy(net_op_chopped, (const char *) network_operator, 10);
  net_op_chopped[5] = '.';
  net_op_chopped[6] = '.';
  net_op_chopped[7] = '\0';

  display.print(F(net_op_chopped));

  // signal strength
  mapped_signal = map(signal_strength, 0, 25, 0, 35);
  display.drawRect(90, 0, 35, 5, signalCol);
  display.fillRect(90, 0, mapped_signal, 5, signalCol);

  // battery capacity
  mapped_battery = map(battery_capacity, 0, 99, 0, 34);
  display.drawRect(90, 8, 35, 10, batteryCol);
  display.fillRect(90, 8, mapped_battery, 10, batteryCol);

  display.setTextColor(red);

  draw_jamming_icon();
}

/*
   Function: draw_jamming_icon
   ----------------------------
     Draws a signal look alike icon,
     if no jamming, green color
     if jamming, red color
     else its color is white
*/
void draw_jamming_icon() {
  // jamming detection
  uint8_t j_d_offset_x  = 105;
  uint8_t j_d_offset_y  = 0; // already 30 down
  Color jammingColor;
  if (jamming_detected == 0) jammingColor = green;
  else if (jamming_detected == 1) jammingColor = red;
  else jammingColor = white;
  display.drawLine( j_d_offset_x , j_d_offset_y + 30, j_d_offset_x  + 5, j_d_offset_y + 30 , jammingColor);
  display.drawLine( j_d_offset_x  + 5, j_d_offset_y + 30, j_d_offset_x  + 8, j_d_offset_y + 20, jammingColor);
  display.drawLine( j_d_offset_x  + 8, j_d_offset_y + 20, j_d_offset_x  + 10, j_d_offset_y + 37, jammingColor);
  display.drawLine( j_d_offset_x  + 10, j_d_offset_y + 37, j_d_offset_x  + 13, j_d_offset_y + 25, jammingColor);
  display.drawLine( j_d_offset_x  + 13, j_d_offset_y + 25, j_d_offset_x  + 15, j_d_offset_y + 30, jammingColor);
  display.drawLine( j_d_offset_x  + 15, j_d_offset_y + 30, j_d_offset_x  + 20, j_d_offset_y + 30, jammingColor);
}


/*
   Function: draw_call_in_prg_screen
   ----------------------------
     Displays upper screen and a
     rectangular in the lower part
     where caller's number (and caller's
     name if stored in SIM) is printed

*/
void draw_call_in_prg_screen() {
  uint8_t s_offset_x  = 95;
  uint8_t s_offset_y  = 30;
  Color stateCol = ssd1351::RGB(110, 25, 25);
  Color backgroundColor = ssd1351::RGB(100, 100, 150);

  draw_upper_screen();

  display.fillCircle(s_offset_x, s_offset_y, 5, stateCol);
  display.setTextColor(white);
  display.fillRoundRect(0, 45, 128, 75, 5, backgroundColor);
  display.setFont(OLED_FONT_BOLD);
  display.setCursor(0, 65);
  if (caller_number[0] != 0) {
    char inc_call_name[CALL_NAME_MAX_LENGTH] = {'\0'};
    strncat(inc_call_name, (const char *) caller_name, CALL_NAME_MAX_LENGTH);
    inc_call_name[7] = '.';
    inc_call_name[8] = '.';
    inc_call_name[9] = '\0';
    display.print(inc_call_name);
    char inc_call_number[11] = { caller_number[0], caller_number[1], caller_number[2],
                                 caller_number[3], caller_number[4], caller_number[5],
                                 caller_number[6], caller_number[7], caller_number[8],
                                 caller_number[9], '\0'
                               };
    display.setCursor(0, 85);
    display.print(inc_call_number);
  }
  display.setFont(OLED_FONT);
  display.updateScreen();
}


/*
   Function: draw_typing_number_screen
   ----------------------------
     Displays upper screen and a
     rectangular in the lower part
     where a 10 digit number can be typed
     in order to later on be called

*/
void draw_typing_number_screen() {
  uint8_t s_offset_x  = 95;
  uint8_t s_offset_y  = 30;
  Color stateCol = ssd1351::RGB(255, 255, 255);
  Color backgroundColor = ssd1351::RGB(100, 100, 150);

  draw_upper_screen();

  display.fillCircle(s_offset_x, s_offset_y, 5, stateCol);

  display.setTextColor(white);
  display.fillRoundRect(0, 45, 128, 75, 5, backgroundColor);

  display.setCursor(0, 60);
  display.setTextColor(blue);
  display.print(typingNumberStr);

  display.setFont(OLED_FONT_BOLD);
  display.setTextColor(red);
  display.setCursor(number_current_digit * 10, 80);
  display.print(pot_value);
  display.setFont(OLED_FONT);

  display.updateScreen();
}

/*
   Function: draw_inc_call_screen
   ----------------------------
     Displays upper screen and a
     rectangular in the lower part
     where caller's number (and
     caller's name if stored in SIM)
     is displayed

*/
void draw_inc_call_screen() {
  draw_upper_screen();
  uint8_t s_offset_x  = 95;
  uint8_t s_offset_y  = 30;
  Color stateCol = ssd1351::RGB(110, 25, 25);
  Color backgroundColor = ssd1351::RGB(100, 100, 150);
  char inc_call_name[CALL_NAME_MAX_LENGTH] = {'\0'};
  char inc_call_number[11] = { caller_number[0], caller_number[1], caller_number[2],
                               caller_number[3], caller_number[4], caller_number[5],
                               caller_number[6], caller_number[7], caller_number[8],
                               caller_number[9], '\0'
                             };

  display.fillCircle(s_offset_x, s_offset_y, 5, stateCol);

  display.setTextColor(white);
  display.fillRoundRect(0, 45, 128, 75, 5, backgroundColor);
  display.setFont(OLED_FONT_BOLD);
  display.setCursor(0, 65);
  strncat(inc_call_name, (const char *) caller_name, CALL_NAME_MAX_LENGTH);
  display.print(inc_call_name);
  display.setCursor(0, 85);
  display.print(inc_call_number);
  display.setFont(OLED_FONT);

  display.updateScreen();
}

/*
   Function: draw_idle_screen
   ----------------------------
     Displays upper screen. By default a moving
     animation is drawn.
     If a missed call, or if the appropriate
     button is pressed, a past call number is
     displayed in order to be later on called
     or changed.

*/
void draw_idle_screen() {
  uint8_t s_offset_x  = 95;
  uint8_t s_offset_y  = 30;
  Color stateCol = ssd1351::RGB(25, 110, 25);

  draw_upper_screen();

  display.fillCircle(s_offset_x, s_offset_y, 5, stateCol);
  if (showing_past_calls) {
    if (names_after_boot[past_calls_index][0] != '\0') {
      display.setFont(OLED_FONT_BOLD);
      display.setCursor(5, 65);
      display.print(F((char *)names_after_boot[past_calls_index]));
      display.setFont(OLED_FONT);
    }
    if (numbers_after_boot[past_calls_index][0] != '\0') {
      display.setCursor(5, 85);
      display.print(F((char *)numbers_after_boot[past_calls_index]));
    }
  } else {
    draw_animated_circles();
  }
  display.updateScreen();
}


/*
   Function: draw_animated_circles
   ----------------------------
     Draws 3 circles of different diameters
     which increase and decrease while time
     passes (something like a screen-saver).

*/
void draw_animated_circles() {
  Color stateCol = ssd1351::RGB(25, 110, 25);
  static uint8_t counter = 2;
  static uint8_t growing = 1;
  if (growing == 1) {
    counter += 1;
    if (counter >= 40) growing = 0;
  } else {
    counter -= 1;
    if (counter <= 20) growing = 1;
  }
  display.fillCircle(55, 85, counter, stateCol);
  display.fillCircle(55, 85, counter - 1, white);
  display.fillCircle(55, 85, counter - 2, stateCol);
  if (counter >= 12) {
    display.fillCircle(85, 85, counter - 10, stateCol);
    display.fillCircle(85, 85, counter - 11, white);
    display.fillCircle(85, 85, counter - 12, stateCol);
  }
  if (counter >= 22) {
    display.fillCircle(25, 65, counter - 20, stateCol);
    display.fillCircle(25, 65, counter - 21, white);
    display.fillCircle(25, 65, counter - 22, stateCol);
  }
  if (counter >= 25) {
    display.fillCircle(30, 105, counter - 23, stateCol);
    display.fillCircle(30, 105, counter - 24, white);
    display.fillCircle(30, 105, counter - 25, stateCol);
  }
}


/*
   Function: init_oled
   ----------------------------
     Calls ssd1351 library's initializer,
     sets font & text size

*/
void init_oled(void) {
  display.begin();
  display.setFont(OLED_FONT);
  display.setTextSize(1);
}
