/*
  configTICK_RATE_HZ = 1000, FreeRTOS-Teensy4/src/FreeRTOSConfig_default.h
*/
static void ThreadOLED(void* arg);
static void ThreadPOT(void* arg);
static void ThreadGSM(void* arg);
static void ThreadButtons(void* arg);

void idle_commands(void);
void retrieving_number_commands(void);
void typing_number_commands(void);
void incoming_call_commands(void);
void call_in_prg_commands(void);
void cache_number(char * number);
void cache_last_call(void);

/*
   Function: ThreadPOT
   ----------------------------
     Thread responsible for
     reading potentiometer's voltage,
     maps it to 0-9, and stores the
     result on "pot_value" shared var.

     Period: TPOT_MS
*/
static void ThreadPOT(void* arg) {
  while (1) {
    xSemaphoreTake(sem1, portMAX_DELAY);
    int value = get_pot();
    int mapped = map(value, 0, 1023, 0, 9);
    pot_value = mapped;
    xSemaphoreGive(sem1);
    vTaskDelay((TPOT_MS * configTICK_RATE_HZ) / 1000L);
  }
}

/*
   Function: ThreadOLED
   ----------------------------
     Thread responsible for
     calling appropriate draw function
     for display purposes,

     Period: TOLED_MS
*/
static void ThreadOLED(void* arg) {
  while (1) {
    xSemaphoreTake(sem1, portMAX_DELAY);
    switch (state) {
      case IDLE: // idle screen
        draw_idle_screen();
        break;
      case CALL_IN_PRG: // in call screen
        draw_call_in_prg_screen();
        break;
      case TYPING_NUMBER: // typing number screen
        draw_typing_number_screen();
        break;
      case RETRIEVING_NUMBER: // retrieving number
        break;
      case INCOMING_CALL: // incoming call
        draw_inc_call_screen();
        break;
      case UNLOCKING_SIM: // incoming call
        draw_unlock_screen();
        break;
      default:
        draw_idle_screen();
    }
    xSemaphoreGive(sem1);
    vTaskDelay((TOLED_MS * configTICK_RATE_HZ) / 1000L);
  }
}

/*
   Function: ThreadButtons
   ----------------------------
     Thread responsible for
     reading buttons' states.

     Buttons change behaviour relatively
     to "state" shared variable:
     IDLE: left button shows past call,
           right button changes to TYPING_NUMBER
             state.
     INCOMING_CALL: left button accepts call,
                    right button hangs it.
     CALL_IN_PRG: left button accepts call,
                    right button hangs it.
     TYPING_NUMBER: left button moves digit-to-be
                      changed 1 position left.
                    right button 1 position right,
                      if digit 10, a call is made.
                    top button returns to IDLE.

     Period: TBUTTONS_MS
*/
static void ThreadButtons(void* arg) {
  while (1) {
    uint8_t pressed_button = update_buttons();


    if (state == UNLOCKING_SIM && pressed_button == 1) {
      String simStr = String((char *)sim_pin);
      if (!simStr.equals(F("0000"))) {
        state = IDLE;
        Serial2.begin(19200);
        vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);
        unlock_sim((char *) sim_pin, (char *) gsm_reply);
        vTaskDelay((5000 * configTICK_RATE_HZ) / 1000L);
        xSemaphoreTake(sem1, portMAX_DELAY);
        set_jamming_detection((char *)gsm_reply);
        xSemaphoreGive(sem1);
        vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);
        vTaskResume(xHandleGSM);
      }
    } else if (state == UNLOCKING_SIM && pressed_button == 4) {
      sim_pin[sim_pin_digit] = '0' + (char)pot_value;
      sim_pin[4] = '\0';
    } else if (state == UNLOCKING_SIM && pressed_button == 2) {
      if (sim_pin_digit < 3) sim_pin_digit += 1;
    } else if (state == UNLOCKING_SIM && pressed_button == 0) {
      if (sim_pin_digit > 0) sim_pin_digit -= 1;
    }

    if (state == IDLE && pressed_button == 0) {

      showing_past_calls = true;
      if (past_calls_index > 0) {
        if (numbers_after_boot[past_calls_index - 1][0] != '\0') past_calls_index -= 1;
      }
    } else if (state == TYPING_NUMBER && pressed_button == 0) {
      if (number_current_digit > 0) number_current_digit -= 1;
      else number_current_digit = 0;
    } else if (state == INCOMING_CALL && pressed_button == 0) {
      acceptCall((char *)gsm_reply);
    }

    if (state == IDLE && pressed_button == 1) {
      showing_past_calls = false;
    } else if (state == TYPING_NUMBER && pressed_button == 1) {
      showing_past_calls = false;
      state = IDLE;
    }

    if (state == TYPING_NUMBER && pressed_button == 2) {
      if (number_current_digit < 9) {
        number_current_digit += 1;
      } else {
        char number[11];
        cache_number(number);
        number[10] = '\0';
        xSemaphoreTake(sem1, portMAX_DELAY);
        make_call(number, (char *)gsm_reply);
        xSemaphoreGive(sem1);
      }
    } else if (state == INCOMING_CALL && pressed_button == 2) {
      xSemaphoreTake(sem1, portMAX_DELAY);
      hang_call((char *)gsm_reply);
      xSemaphoreGive(sem1);
    } else if (state == CALL_IN_PRG && pressed_button == 2) {
      xSemaphoreTake(sem1, portMAX_DELAY);
      hang_call((char *)gsm_reply);
      xSemaphoreGive(sem1);
    } else if (state == IDLE && pressed_button == 2 && showing_past_calls) {
      if (past_calls_index < MAX_STORED_NUMBERS - 1) {
        if (numbers_after_boot[past_calls_index + 1][0] != '\0') past_calls_index += 1;
      }
      cache_last_call();
    } else if (state == IDLE && pressed_button == 2 && !showing_past_calls) {
      typingNumberStr = "";
      for (int i = 0; i < 10; i++) {
        typingNumberStr += String(typing_number[i]);
      }
      state = TYPING_NUMBER;
    }

    if (state == TYPING_NUMBER && pressed_button == 4) {
      typingNumberStr = "";
      typing_number[number_current_digit] = pot_value;
      for (int i = 0; i < 10; i++) {
        typingNumberStr += String(typing_number[i]);
      }
      if (number_current_digit <= 8) number_current_digit += 1;
    } else if (state == IDLE && pressed_button == 4  && showing_past_calls == true) {
      cache_last_call();
      number_current_digit = 9;
      showing_past_calls = false;
      if (typingNumberStr[0] != '\0') {
        char number[11];
        cache_number(number);
        xSemaphoreTake(sem1, portMAX_DELAY);
        make_call(number, (char *)gsm_reply);
        xSemaphoreGive(sem1);
      }
    }

    vTaskDelay((TBUTTONS_MS * configTICK_RATE_HZ) / 1000L);
  }
}

/*
   Function: ThreadGSM
   ----------------------------
     Thread responsible for
     sending AT commands to SIM800L
     module. Different commands are sent
     in each state.

     Period: not stable, delays are relative
     to state.
*/
static void ThreadGSM(void* arg) {
  while (1) {
    switch (state) {
      case IDLE:
        idle_commands();
        break;
      case RETRIEVING_NUMBER:
        retrieving_number_commands();
        break;
      case TYPING_NUMBER:
        typing_number_commands();
        break;
      case INCOMING_CALL:
        if (!showing_past_calls) showing_past_calls = true;
        incoming_call_commands();
        break;
      case CALL_IN_PRG:
        call_in_prg_commands();
        break;
      default:
        idle_commands();
    }
  }
}

/*
   Function: idle_commands
   ----------------------------
     Calls appropriate functions to read
     the following:
      signal strength,
      battery capacity,
      cell network operator,
      jamming detection,
      activity status
     from SIM800L module.

     (called when state == IDLE)
*/
void idle_commands() {
  xSemaphoreTake(sem1, portMAX_DELAY);
  get_signal_strength((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_battery_capacity((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_operator((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_jamming((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_sim_activity((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);
}

/*
   Function: retrieving_number_commands
   ----------------------------

     (called when state == RETRIEVING_NUMBER)
*/
void retrieving_number_commands() {

}

/*
   Function: typing_number_commands
   ----------------------------

     (called when state == TYPING_NUMBER)
*/
void typing_number_commands() {

}

/*
   Function: incoming_call_commands
   ----------------------------
     Calls appropriate functions to read
     the following:
      caller number,
      signal strength,
      jamming detection,
      activity status,
      battery capacity every 5"
     from SIM800L module.

     (called when state == INCOMING_CALL)
*/
void incoming_call_commands() {
  static unsigned long last_time = 0;
  xSemaphoreTake(sem1, portMAX_DELAY);
  get_caller((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_signal_strength((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_jamming((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_sim_activity((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  if (millis() - last_time > 5000) {
    xSemaphoreTake(sem1, portMAX_DELAY);
    get_battery_capacity((char *)gsm_reply);
    xSemaphoreGive(sem1);
    vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);
    last_time = millis();
  }
}

/*
   Function: incoming_call_commands
   ----------------------------
     Calls appropriate functions to read
     the following:
      signal strength,
      jamming detection,
      activity status,
      caller number every 5",
      battery capacity every 5"
     from SIM800L module.

     (called when state == CALL_IN_PRG)
*/
void call_in_prg_commands() {
  static unsigned long last_time = 5001;
  if (millis() - last_time > 5000) {
    xSemaphoreTake(sem1, portMAX_DELAY);
    get_caller((char *)gsm_reply);
    xSemaphoreGive(sem1);
    vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

    xSemaphoreTake(sem1, portMAX_DELAY);
    get_battery_capacity((char *)gsm_reply);
    xSemaphoreGive(sem1);
    vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);
    last_time = millis();
  }
  xSemaphoreTake(sem1, portMAX_DELAY);
  get_signal_strength((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_jamming((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

  xSemaphoreTake(sem1, portMAX_DELAY);
  get_sim_activity((char *)gsm_reply);
  xSemaphoreGive(sem1);
  vTaskDelay((TGSM_COM_MS * configTICK_RATE_HZ) / 1000L);

}

/*
   Function: cache_number
   ----------------------------
     Stores a sequence of 10 characters
     to shared vars

     number: pointer to a (calling) number
     stored in char array

*/
void cache_number(char * number) {
  typingNumberStr.toCharArray(number, 11);
  number[10] = '\0';
  for (int i = 0; i < 10; i++) {
    typing_number[i] = number[i] - '0';
  }
  strncpy((char*)caller_number, (const char*) number, CALL_NUMBER_MAX_LENGTH);
  strncpy((char*)caller_name, (const char*) F(""), CALL_NAME_MAX_LENGTH);
}

/*
   Function: cache_last_call
   ----------------------------
     Stores a sequence of 10 characters
     to typing_number shared var only.

*/
void cache_last_call() {
  typingNumberStr = String((char *)numbers_after_boot[past_calls_index]);
  for (int i = 0; i < 10; i++) {
    if (numbers_after_boot[past_calls_index][0] == '+' ) {
      typing_number[i] = numbers_after_boot[past_calls_index][i + 3] - '0';
    } else {
      typing_number[i] = numbers_after_boot[past_calls_index][i] - '0';
    }
  }
}

/*
   Function: init_threads
   ----------------------------
     Creates FreeRTOS 4 tasks, 1 for 
     each declared Thread on top of 
     this file. The thread which is 
     responsible for SIM800's 
     communication is suspended until 
     a SIM PIN has been entered, 
     after the device's boot.

*/
void init_threads(void) {
  portBASE_TYPE s1, s2, s3, s4;

  sem1 = xSemaphoreCreateCounting(1, 0);

  s1 = xTaskCreate(ThreadGSM, NULL, configMINIMAL_STACK_SIZE, NULL, 1, &xHandleGSM);
  s2 = xTaskCreate(ThreadOLED, NULL, configMINIMAL_STACK_SIZE, NULL, 2, NULL);
  s3 = xTaskCreate(ThreadPOT, NULL, configMINIMAL_STACK_SIZE, NULL, 3, NULL);
  s4 = xTaskCreate(ThreadButtons, NULL, configMINIMAL_STACK_SIZE, NULL, 3, NULL);

  if (sem1 == NULL || s1 != pdPASS || s2 != pdPASS || s3 != pdPASS || s4 != pdPASS) {
    while (1);
  }
  vTaskSuspend(xHandleGSM);
  xSemaphoreGive(sem1);
}

void init_rtos_scheduler(void) {
  vTaskStartScheduler();
  while (1);
}
