#include <Arduino.h>
#include <SPI.h>
#include <FreeRTOS_TEENSY4.h>
#include <ssd1351.h>
#include "definitions.h" /* 1st include */
#include "shared.h" /* 2nd include */
#include "GSM.h"
#include "Buttons.h"
#include "OLED.h"
#include "Threads.h"

void setup(void);
void loop(void);

void setup(void) {
  init_oled();
  init_gsm();
  init_buttons();

  //FreeRTOS
  init_threads();
  init_rtos_scheduler();
}

void loop(void) {
  // Not used.
}
