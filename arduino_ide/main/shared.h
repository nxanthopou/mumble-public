
volatile int typing_number[10] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
volatile char sim_pin[5] = "0000";
volatile uint8_t sim_pin_digit = 0;

volatile char numbers_after_boot[MAX_STORED_NUMBERS][CALL_NUMBER_MAX_LENGTH];
volatile char names_after_boot[MAX_STORED_NUMBERS][CALL_NAME_MAX_LENGTH];

volatile phone_state state;
volatile char gsm_reply[GSM_BUFF_SIZE];

volatile uint8_t signal_strength;
volatile uint8_t battery_capacity;
volatile uint8_t jamming_detected;
volatile int pot_value;
volatile char network_operator[NET_OP_MAX_LENGTH];
volatile char caller_number[CALL_NUMBER_MAX_LENGTH];
volatile char caller_name[CALL_NAME_MAX_LENGTH];

uint8_t past_calls_index = 0; /* since boot */
volatile boolean showing_past_calls = false;
volatile uint8_t number_current_digit = 0; /* typing number digit */
String typingNumberStr = "";

SemaphoreHandle_t sem1;
TaskHandle_t xHandleGSM;

auto display = ssd1351::SSD1351<Color, ssd1351::SingleBuffer, OLED_XPIXELS, OLED_YPIXELS>();

Color white = ssd1351::RGB(255, 255, 255);
Color black = ssd1351::RGB(0, 0, 0);
Color red = ssd1351::RGB(255, 0, 0);
Color green = ssd1351::RGB(0, 255, 0);
Color blue = ssd1351::RGB(0, 0, 255);
Color yellow = ssd1351::RGB(255, 255, 0);
Color grayBlue = ssd1351::RGB(102, 102, 153);
