void send_at_command(char * at_command, char * result);
void make_call(char * number, char * result);
void hang_call(char * result);
void get_signal_strength(char * result);
void get_operator(char * result);
void get_jamming(char * result);
void set_jamming_detection(char * result);
void get_caller(char * result);
void get_sim_activity(char * result);
uint8_t parse_battery_capacity(char * result);
uint8_t parse_signal_strength(char * result);
uint8_t parse_jamming(char * result);
uint8_t parse_sim_activity(char * result);
void parse_operator(char * result);
int parse_command_to_int(String extractedCommand);
void read_reply(char * reply);
void unlock_sim(char * PIN, char * result);
void add_number_to_list(void);

/*
   Function: parse_clcc
   ----------------------------
     Reply to AT command "AT+CLCC".
     Stores caller's name and number in shared vars.
      caller_number: char[CALL_NUMBER_MAX_LENGTH]
      caller_name: char[CALL_NAME_MAX_LENGTH]
  
     Example reply:
     +CLCC: 1,1,4,0,0,"+{code}{number}",145,"{name}"
  
     Notes:
      removes country code
      name exists only if the caller is stored as
       phone entry in SIM

*/
void parse_clcc(char * result) {
  char call_numb_arr[CALL_NUMBER_MAX_LENGTH];
  char call_name_arr[CALL_NAME_MAX_LENGTH];
  call_numb_arr[0] = '\0';
  call_name_arr[0] = '\0';
  String resultStr = String(result);
  int delim_index1 = resultStr.indexOf("\"");
  if (resultStr.length() < 15) return; // "hardcoded" fix to avoid other at_commands
  String subStringNum = resultStr.substring(delim_index1, delim_index1 + 14);
  subStringNum.replace("\"", "");

  if (subStringNum == "") return; // <- check this
  if (subStringNum[0] == '+') {
    subStringNum[1] = '+';
    subStringNum[2] = '+';
    subStringNum.replace("+", "");
  }
  subStringNum.toCharArray(call_numb_arr, CALL_NUMBER_MAX_LENGTH);

  strncpy((char*)caller_number, (const char*)call_numb_arr, CALL_NUMBER_MAX_LENGTH);
  int delim_index2 = resultStr.lastIndexOf(",");
  int delim_index3 = resultStr.lastIndexOf("\"");
  String subStringName = resultStr.substring(delim_index2, delim_index3 + 1);
  subStringName.replace(",", "");
  subStringName.replace("\"", "");

  subStringName.toCharArray(call_name_arr, CALL_NAME_MAX_LENGTH);
  strncpy((char*)caller_name, (const char*)call_name_arr, CALL_NAME_MAX_LENGTH);

  add_number_to_list();
}


/*
   Function: get_caller
   ----------------------------
     Sends "AT+CLCC" command (list current
     calls) to SIM800.
     p.g. 72 SIM800 AT command manual

*/
void get_caller(char * result) {
  char at_command[8] = "AT+CLCC";
  send_at_command(at_command, result);
}


/*
   Function: get_sim_activity
   ----------------------------
     Sends "AT+CPAS" command to SIM800
     (phone activity status)
     p.g. 81 SIM800 AT command manual

*/
void get_sim_activity(char * result) {
  char at_command[8] = "AT+CPAS";
  send_at_command(at_command, result);
}


/*
   Function: parse_sim_activity
   ----------------------------
     Reply to "AT+CPAS" AT command.
      0 for idle/ready
      2 for unknown
      3 for incoming call
      4 for call in progress
  
     Returns the activity status if
     the command was parsed normally,
     else last state.

*/
uint8_t parse_sim_activity(char * result) {
  String resultStr = String(result);
  int8_t string_length = resultStr.length();
  int delim_index = resultStr.indexOf(":");
  if (delim_index != -1 && string_length >= delim_index + 1) {
    String subString1 = resultStr.substring(delim_index, string_length);
    subString1.replace(":", "");
    uint8_t activity = (uint8_t) subString1.toInt();
    return activity;
  }
  return (uint8_t) state;
}

/*
   Function: init_gsm
   ----------------------------
     Initializes SIM800 related shared variables
      state: phone_state typedef
      jamming_detected: uint8_t
      caller_number: char[CALL_NUMBER_MAX_LENGTH]
      caller_name: char[CALL_NAME_MAX_LENGTH]
      numbers_after_boot: char[MAX_STORED_NUMBERS][CALL_NUMBER_MAX_LENGTH]
      names_after_boot: char[MAX_STORED_NUMBERS][CALL_NAME_MAX_LENGTH]

*/
void init_gsm(void) {
  state = UNLOCKING_SIM;
  jamming_detected = 0;
  caller_number[0] = '\0';
  caller_name[0] = '\0';
  numbers_after_boot[0][0] = '\0';
  names_after_boot[0][0] = '\0';
}

/*
   Function: unlock_sim
   ----------------------------
     Sends "AT+CPIN={SIM PIN}" command
     (unlocks sim)
  
     p.g. 86 SIM800 AT command manual

*/
void unlock_sim(char * pin, char * reply) {
  char at_command[13] = "AT+CPIN=";
  strncat(at_command, pin, 4);
  send_at_command(at_command, reply);
}

/*
   Function: set_jamming_detection
   ----------------------------
     Sends "AT+SJDR={settings}" command
     (sets jamming detection)
  
     p.g. 190 SIM800 AT command manual

*/
void set_jamming_detection(char * result) {
  char at_command[18] = "AT+SJDR=1,0,255,1";
  send_at_command(at_command, result);
}

/*
   Function: make_call
   ----------------------------
     Sends "AT+ATD{number};" command
     (mobile originated call to dial
     a number)
  
     p.g. 29 SIM800 AT command manual

*/
void make_call(char * number, char * result) {
  char at_command[15] = "ATD";
  strncat(at_command, number, 10);
  at_command[13] = ';';
  at_command[14] = '\0';
  send_at_command(at_command, result);
}


/*
   Function: hang_call
   ----------------------------
     Sends "ATH" command
     (disconnect existing connection)
  
     p.g. 35 SIM800 AT command manual

*/
void hang_call(char * result) {
  char at_command[4] = "ATH";
  send_at_command(at_command, result);
  state = IDLE;
}


/*
   Function: hang_call
   ----------------------------
     Sends "ATA" command
     (answer an incoming call)
  
     p.g. 28 SIM800 AT command manual

*/
void acceptCall(char * result) {
  char at_command[4] = "ATA";
  send_at_command(at_command, result);
}


/*
   Function: get_jamming
   ----------------------------
     Sends "AT+SJDR?" command
     (set jamming detection function).
  
  
     p.g. 28 SIM800 AT command manual

*/
void get_jamming(char * result) {
  char at_command[9] = "AT+SJDR?";
  send_at_command(at_command, result);
}

/*
   Function: parse_jamming
   ----------------------------
     Reply to "AT+SJDR?" command.
  
     Example reply:
     +SJDR: 1,0,255,1,0
  
     p.g. 189 SIM800 AT command manual

*/
uint8_t parse_jamming(char * result) {
  String resultStr = String(result);
  int8_t stringLength = resultStr.length();
  int delim_index = resultStr.lastIndexOf(",");
  if (delim_index != -1 && stringLength >= delim_index + 1) {
    String subString1 = resultStr.substring(delim_index, stringLength);
    subString1.replace(",", "");
    uint8_t jamming = (uint8_t) subString1.toInt();
    return jamming;
  }
  return 255;
}

/*
   Function: get_operator
   ----------------------------
     Sends "AT+CPOS?" command
     (operator selection)
  
     p.g. 80 SIM800 AT command manual

*/
void get_operator(char * reply) {
  char at_command[9] = "AT+COPS?";
  send_at_command(at_command, reply);
}


/*
   Function: parse_jamming
   ----------------------------
     Reply to "AT+CPOS?" command.
  
     Example reply:
     +COPS: 0,0,"{operator}"

*/
void parse_operator(char * reply) {
  String replyStr = String(reply);
  int8_t stringLength = replyStr.length();
  int delim_index = replyStr.indexOf("\"");
  if (delim_index != -1 && stringLength > 5) {
    String subString1 = replyStr.substring(delim_index, stringLength - 1);
    int secondDelimInidex = subString1.indexOf("\"");
    String subString2 = subString1.substring(0, secondDelimInidex - 1);
    subString2.replace("\"", "");
    subString2.replace("OK", "");
    subString2.replace("\n", "");
    char casted[20];
    casted[0] = '\0';
    subString2.toCharArray(casted, 20);
    strncpy((char *)network_operator, (const char *)casted, NET_OP_MAX_LENGTH);
  } else {
    strncpy((char *)network_operator, "None", NET_OP_MAX_LENGTH);
  }
}


/*
   Function: get_operator
   ----------------------------
     Sends "AT+CBC" command
     (battery charge)
  
     p.g. 108 SIM800 AT command manual

*/
void get_battery_capacity(char * reply) {
  char at_command[7] = "AT+CBC";
  send_at_command(at_command, reply);
}


/*
   Function: parse_battery_capacity
   ----------------------------
     Reply to "AT+CBC" command.
  
     Example reply:
     +CBC: 0,85,4081

*/
uint8_t parse_battery_capacity(char * reply) {
  String replyStr = String(reply);
  int delim_index = replyStr.indexOf(",");
  int8_t stringLength = replyStr.length();
  if (delim_index != -1 && stringLength > delim_index + 4) {
    String subStr = replyStr.substring(delim_index + 1, delim_index + 4);
    subStr.replace(",", "");
    int castedInt = subStr.toInt();
    return (uint8_t) castedInt;
  }
  return 0;

}

/*
   Function: get_signal_strength
   ----------------------------
     Sends "AT+CSQ" command
     (signal quality report)
  
     p.g. 93 SIM800 AT command manual

*/
void get_signal_strength(char * reply) {
  char at_command[7] = "AT+CSQ";
  send_at_command(at_command, reply);
}



/*
   Function: parse_signal_strength
   ----------------------------
     Reply to "AT+CSQ" command.
  
     Example reply:
     +CSQ: 17,0

*/
uint8_t parse_signal_strength(char * reply) {
  String replyStr = String(reply);
  int delim_index = replyStr.indexOf(",");
  int8_t stringLength = replyStr.length();
  if (delim_index != -1 && stringLength > delim_index - 2 ) {
    String subStr = replyStr.substring(delim_index - 2, delim_index);
    int ssInt = subStr.toInt();
    return (uint8_t) ssInt;
  }
  return 0;
}


/*
   Function: send_at_command
   ----------------------------
     Sends an already formed AT command
     through Teensy's Serial pins,
     reads the reply and passes a pointer
     to the stored location

*/
void send_at_command(char * at_command, char * reply) {
  Serial2.println(F(at_command));
  read_reply(reply);
}

/*
   Function: read_reply
   ----------------------------
     Reads SIM800's reply through
     Serial pins, extracts the command
     which they correspond to,
     sets related shared variables.
  
     battery_capacity: uint8_t
     signal_strength: uint8_t
     jamming_detected: uint8_t
     state: phone_activity typedef

*/
void read_reply(char * reply) {
  reply[0] = '\0';
  uint8_t bytesReceived = 0; /* https://www.pjrc.com/teensy/td_Serial.html USB Buffering and Timing Details */
  while (Serial2.available() && bytesReceived < 64) {
    char inc_byte = Serial2.read();
    char * casted = (char *) &inc_byte;
    strncat(reply, casted, 1);
    bytesReceived += 1;
  }
  String replyStr = String(reply);
  // maybe receive "OK" as command failed/succeeded
  // alongside with "ERROR"
  // instead of just removing it

  replyStr.replace("OK", "");
  if (replyStr.length() < 2)  {
    return;
  }
  String extractedCommand = replyStr.substring(0, 6);
  int command_int = parse_command_to_int(extractedCommand);
  switch (command_int) {
    case 1:
      battery_capacity = parse_battery_capacity(reply);
      break;
    case 2:
      signal_strength = parse_signal_strength(reply);
      break;
    case 3:
      parse_operator(reply);
      break;
    case 4:
      jamming_detected = parse_jamming(reply);
      break;
    case 5:
      parse_clcc(reply);
      break;
    case 6: {
        uint8_t gsm_activity = parse_sim_activity(reply);
        if (state != (uint8_t) INCOMING_CALL && gsm_activity == (uint8_t) INCOMING_CALL) {
          // else last number stays in cache until CLCC AT command returns
          // caller
          strncpy((char*)caller_number, (const char*) F(""), CALL_NUMBER_MAX_LENGTH);
          strncpy((char*)caller_name, (const char*) F(""), CALL_NAME_MAX_LENGTH);
        }
        state = (phone_state) gsm_activity;
      }
      break;
    case 7: {
        // sim unlock CPIN
      }
      break;
    default:
      //Serial.println(replyStr);
      return;
  }
}

/*
   Function: parse_command_to_int
   ----------------------------
     read_reply's helper function, maps
     extracted commands to an int.

*/
int parse_command_to_int(String extractedCommand) {
  if (extractedCommand == F("AT+CBC")) {
    return 1;
  } else if (extractedCommand == F("AT+CSQ")) {
    return 2;
  } else if (extractedCommand == F("AT+COP")) {
    return 3;
  } else if (extractedCommand == F("AT+SJD")) {
    return 4;
  } else if (extractedCommand == F("AT+CLC")) {
    return 5;
  } else if (extractedCommand == F("AT+CPA")) {
    return 6;
  } else if (extractedCommand == F("AT+CPI")) {
    return 7;
  }
  return -1;
}

/*
   Function: add_number_to_list
   ----------------------------
     Pushes current caller's number to a
     list, in order to store missed calls
  
     Shared variables:
      numbers_after_boot: char[MAX_STORED_NUMBERS][CALL_NUMBER_MAX_LENGTH]
      names_after_boot: char[MAX_STORED_NUMBERS][CALL_NAME_MAX_LENGTH]
*/
void add_number_to_list() {
  if (caller_number != numbers_after_boot[0]) {
    strncpy((char *)numbers_after_boot[1], (const char*) numbers_after_boot[0], sizeof(numbers_after_boot) - CALL_NUMBER_MAX_LENGTH);
    strncpy((char *)numbers_after_boot[0], (const char*) caller_number, CALL_NUMBER_MAX_LENGTH);
    strncpy((char *)names_after_boot[1], (const char*) names_after_boot[0], sizeof(names_after_boot) - CALL_NAME_MAX_LENGTH);
    strncpy((char *)names_after_boot[0], (const char*) caller_name, CALL_NAME_MAX_LENGTH);
  }
}
