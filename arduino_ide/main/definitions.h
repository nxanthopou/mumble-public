#define HWSERIAL Serial2 // pins 7-8

#define GSM_BUFF_SIZE 64
#define NET_OP_MAX_LENGTH 15
#define MAX_STORED_NUMBERS 10
#define CALL_NUMBER_MAX_LENGTH 11
#define CALL_NAME_MAX_LENGTH 15

#define OLED_YPIXELS 128
#define OLED_XPIXELS 128
#define OLED_FONT FreeSans9pt7b
#define OLED_FONT_BOLD FreeSansBold9pt7b
#define OLED_FONT_BIG FreeSans18pt7b
#define OLED_FONT_BIG_BOLD FreeSansBold18pt7b
#define FONT_PT 9
#define FONT_BIG_PT 18

#define POT_PIN 20
#define BOUNCE_DELAY_MS 10
#define LEFT_BUTTON_PIN 4 // 2
#define TOP_BUTTON_PIN 6 // 3
#define RIGHT_BUTTON_PIN 5 // 4
#define BOTTOM_BUTTON_PIN 2 // 5 
#define CENTER_BUTTON_PIN 3 // 6

#define TOLED_MS 30L /* ~13ms for LowColor, ~24ms for HighColor, as per lib's readme */
#define TPOT_MS 100L
#define TBUTTONS_MS 50L /* bit high, doesnt catch fast pushes */
#define TGSM_COM_MS 100L

typedef enum {
  IDLE,
  RETRIEVING_NUMBER,
  UNKNOWN,
  INCOMING_CALL,
  CALL_IN_PRG,
  TYPING_NUMBER,
  PAST_CALLS,
  UNLOCKING_SIM
} phone_state;

typedef ssd1351::LowColor Color;
