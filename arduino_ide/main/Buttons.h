#include <Bounce2.h>

void init_buttons(void);
uint8_t update_buttons(void);
int get_pot(void);

Bounce leftButton = Bounce(LEFT_BUTTON_PIN, BOUNCE_DELAY_MS);
Bounce topButton = Bounce(TOP_BUTTON_PIN, BOUNCE_DELAY_MS);
Bounce rightButton = Bounce(RIGHT_BUTTON_PIN, BOUNCE_DELAY_MS);
Bounce bottomButton = Bounce(BOTTOM_BUTTON_PIN, BOUNCE_DELAY_MS); // bugs after calls/high curr
Bounce centerButton = Bounce(CENTER_BUTTON_PIN, BOUNCE_DELAY_MS);


/*
   Function: init_buttons
   ----------------------------
    Initializes buttons' pins

*/
void init_buttons(void) {
  pinMode( LEFT_BUTTON_PIN, INPUT_PULLUP);
  pinMode( TOP_BUTTON_PIN, INPUT_PULLUP);
  pinMode( RIGHT_BUTTON_PIN, INPUT_PULLUP);
  pinMode( BOTTOM_BUTTON_PIN, INPUT_PULLUP);
  pinMode( CENTER_BUTTON_PIN, INPUT_PULLUP);
}

/*
   Function: get_pot
   ----------------------------
    Reads analog voltage from POT_PIN
    Returns its value

*/
int get_pot(void) {
  int value;
  value = analogRead(POT_PIN);
  return value;
}

/*
   Function: update_buttons
   ----------------------------
    Reads buttons' states using
    Bounce2 library

*/
uint8_t update_buttons(void) {
  leftButton.update();
  topButton.update();
  rightButton.update();
  bottomButton.update();
  centerButton.update();

  if (leftButton.fell()) {
    return 0;
  }
  if (topButton.fell()) {
    return 1;
  }
  if (rightButton.fell()) {
    return 2;
  }
  if (bottomButton.fell()) {
    return 3;
  }
  if (centerButton.fell()) {
    return 4;
  }

  return 255;
}
